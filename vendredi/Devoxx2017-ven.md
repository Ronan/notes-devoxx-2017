DevoxxFR 2017 - Vendredi
===

Mettre en place sa sécurité et sa gestion d'identité en 2017
---
- Sébastien Blanc (@sebi2706) - Red Hat / Keycloak, Dutch&French , Father, coder, father of coder girl @SaskiaLois , husband, troller, speaker, Open Source.

Slides : http://redhat.slides.com/sblanc/deck-2?token=tc_y0a5a#/

Github : https://github.com/sebastienblanc/devoxxfr

Comment déléguer l'authentification et l'identification dans son projet :
- Eviter à tout prix le DIY (Do It Youself).
- Eviter le couplage fort entre code et framework de sécurité.
- Utiliser un protocole standard mais pas le SAML2, lourd (XML) et ancien 2005.

Le protocole OAUTH2 est plus moderne mais est simplement un protocole de délégation d'accès, sans identification ni authentification. OpenID Connect complète OAUTH2 pour gérer ces 2 points à l'aide de JWT (JSON Web Token - https://jwt.io/).

Keyclock en est une implémentation.

Dans la démo on fait cohabiter des applications Wildfly Swarm en mode fat jar, Springboot et NodeJS sur une même application frontale Angular.

[Notes](Devoxx2017-ven-secuid.md)

Tuez vos branches, faites des Feature Toggles
---
- Morgan Leroi (@MorganLeroi) - Société Générale

Courte présentation de l'intérêt du développement en feature toggles :
- Evite l'enfer des merges.
- Permet de faire du déploiement rapide.
- Permet de faire des mise en production progressives (Suivant des profils ou sur un part du traffic).
Attention à ne pas oublier de supprimer le code mort de l'ancienne feature une fois la nouvelle utilisée dans 100% des cas.

Le Kata Kebab
---
- Romeu Moura (@malk_zameth) - Endless conversation — with friends & compilers — on art, crafts, dialectic, paradigm jump, serendipity.

Github : https://github.com/malk/the-kebab-kata

Essayez de créer un code qui vous ne considérez pas comme étant “legacy”.

Au cours de ce "Hands-on", le speaker simule différentes techniques que l'on peut rencontrer qui vont mener le développeur à écrire du code "legacy".

La session se conclue par un débat ou les développeurs identifient ces techniques et cherchent des moyens de les esquiver.

Pattern Matching en Java (10 ?)
---
- Remi Forax (http://forax.github.io/) - Université Paris Est Marne-la-vallée

Initialement, Java 10 devait introduire les value objects. Sauf que c'est compliqué.
Du coup, ces values objects seront introduits dans la JVM, mais la Java ne permettra pas de l'utiliser. Les autres languages utilisant les JVM se feront les dents avant de le rendre disponible en Java 11.

Du coup, il a fallut trouver un autre truc intéressant dans Java 10 : Le Pattern Matching ie pouvoir faire des `switch/case` sur la classe d'un objet.

[Notes](Devoxx2017-ven-java10.md)

Les 5 questions con(tre) l'agilité et comment y répondre
---
- Meriem El Aaboudi (@melaaboudi) - Responsable de Xebia Mobile
- Laurène Vol-Monnot (@lvolmonn) - Coach Agile - Xebia

L'atelier "magic estimation" permet de faire une macro estimation d'un projet.

Découper les releases suivant un périmètre fonctionnel pour être capable de livrer en continu des choses utiles.

Les story points d'un projet, sont ce qu'est la distance à une course à pied.
Les jour/homme d'un projet, sont ce qu'est votre temps à une même course à pied.
Ne pas confondre la distance avec le temps nécessaire à la parcourir qui pourra varié suivant votre vitesse et des éléments extérieurs.

[Notes](Devoxx2017-ven-agilite.md)
