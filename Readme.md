DevoxxFR 2017
===

[Mercredi](mercredi/Devoxx2017-mer.md)
---
- Préparez-vous à la modularité selon Java 9
- L'API Collector dans tous ses états
- CSS is Awesome!
- Boîte à outils d'investigation des soucis de mémoire
- JUnit-docker

[Jeudi](jeudi/Devoxx2017-jeu.md)
---
- Java 9 modulo les modules
- Comprendre la loi de Conway pour réussir sa transformation DevOps
- Spring Framework 5.0
- Migrating to Microservice Databases: From Relational Monolith to Distributed Data
- Sensibilisation aux bonnes pratiques techniques du Software Craftsmanship : Lego® à la rescousse !
- Vue.js, à l'ombre des géants

[Vendredi](vendredi/Devoxx2017-ven.md)
---
- Mettre en place sa sécurité et sa gestion d'identité en 2017
- Tuez vos branches, faites des Feature Toggles
- Le Kata Kebab
- Pattern Matching en Java (10 ?)
- Les 5 questions con(tre) l'agilité et comment y répondre
