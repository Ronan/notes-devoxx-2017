Java 9 modulo les modules
===
Jean-Michel Doudoux
@jmdoudoux

try with resources
Stack Walking API
java.awt/java.awt.Desktop
ReactiveStream : publication/souscription asynchrone
API Process : ProcessHandle
Fabriques statics de List/Set/Map immutable
Nouvelles méthodes de Optional
Nouvelles méthodes de Stream : takeWhile / dropWhile

Compilateur qui ne supporte plus que 3 version précédantes : 8, 7, 6 (avec warnings sur la 6)
Jar multi version de java : multi release jar file.

JShell

Numero de version :
$MAJOR.$MINOR.$SECURITY.$PATCH
API fournie pour y accéder.

G1 par défaut sur JVM en mode serveur

javadoc html5 avec moteur de recherche

PKCS12 dans le keystore
Support de SHA3
properties en utf-8

jdeprscan : chercher l'utilisation des deprecated.


