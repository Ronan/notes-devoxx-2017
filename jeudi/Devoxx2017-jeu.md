DevoxxFR 2017 - Jeudi
===

Java 9 modulo les modules
---
- Jean-Michel Doudoux (@jmdoudoux) - Java Champion, didacticiel Java et Eclipse, Ju Jitsu et arts martiaux, Delphi, modélisme et figurines

Slides : https://fr.slideshare.net/jmdoudoux/java-9-modulo-les-modules-devoxx-fr-2017

Présentation des nouveautés de Java9 autres que celles liées aux modules.

- Evolutions sur le `try with resources`.
- Stack walking API
- java.awt/java.awt.Desktop
- Publication et souscription asynchrone avec les `ReactiveStream`
- Refonte de l'API `Process` avec les `ProcessHandle`
- Fabriques statics de `List`/`Set`/`Map` immutable
- Nouvelles méthodes sur les `Optional`
- Nouvelles méthodes sur les `Stream` : `takeWhile` / `dropWhile`
- le compilateur ne supportera plus que les 3 version précedantes (et la plus ancienne, donc 6 avec warnings).
- Multi release Jar files : Il sera possible de publier des Jar embarquant du code pour plusieurs version de la JVM.
- Introduction de JShell.
- Introduction d'une API de manipulation du numéro de version de Java qui se rationnalise sous la forme `$MAJOR.$MINOR.$SECURITY.$PATCH`.
- G1 devient de GC par défaut de la JVM si celle-ci est configurée en mode serveur.
- La javadoc passe au HTML5 et fournira un moteur de recherche "client side" en Javascript.
- Côté sécurité ajout du format PKCS12 pour le keystore.
- Support de l'algorithme SHA3.
- Les properties passent en utf-8.

L'outil `jdeprscan` permet de cherche l'utilisation d'API deprecated dans un JAR.

[Notes](Devoxx2017-jeu-java9.md)

Comprendre la loi de Conway pour réussir sa transformation DevOps
---
- Clément Rochas (@crochas) - coach agile @ xebia.fr

Slides : https://fr.slideshare.net/crochas/devoxx17-reussir-sa-transformation-devops

La loi de Conway s'énonce ainsi : « les organisations qui définissent des systèmes ... sont contraintes de les produire sous des designs qui sont des copies de la structure de communication de leur organisation. ».

En gros cela signifie que le code produit par une entreprise est à l'image de l'organisation de celle-ci.

Cette loi permet d'expliquer des soucis souvent rencontrés en entreprise :
- Employés spécialisés : SPOF (Single Point Of Failure).
- Equipes spécialisés : Dilution de reponsabilité.
- Ouvriers qualifiés : syndrome du "Tout ressemble à un clou pour qui ne possède qu'un marteau".

Si la division du travail en équipes spécialisées n'est pas souhaitable en terme de vélocité, l'alignement business pur d'une organisation en "feature team" est plus couteuse. Il faut essayer de trouver un juste milieu ou des spécialistes peuvent partager leurs compétences et les employés sont plus responsabilisés sur la réussite de leur projet.

[Notes](Devoxx2017-jeu-conway.md)

Spring Framework 5.0
---
- Stéphane Nicoll (@snicoll) - Proud husband. Software engineer. Passionate and enthusiastic about what I do. Working on @springboot, @springframework & Spring Initializr at @Pivotal.

Slides : https://speakerdeck.com/snicoll/spring-framework-5-dot-0-themes-and-trends

App : https://github.com/snicoll-demos/demo-conference-rating

Spring 5 introduit le support de Java 9, le protocole HTTP/2 et l'API servlet 4.

L'API Spring Reactive introduite dans la version 5 permettra la mise en place de controllers non bloquants supportant beaucoup mieux de fortes montées en charge face à des clients "lents" comme des clients mobiles.

Migrating to Microservice Databases: From Relational Monolith to Distributed Data
---
- Edson Yanaga (@yanaga) - Java Champion & Microsoft MVP. Empowering developers worldwide to deliver better software faster as Director of Developer Experience at Red Hat.

slides : https://speakerdeck.com/yanaga/migrating-to-microservice-databases-from-relational-monolith-to-distributed-data

ebook : https://developers.redhat.com/promotions/migrating-to-microservice-databases/

Présentation de méthodes à mettre en oeuvre pour migrer d'une base de données monolithique à des bases de données distribués. Si la tâche est ardue dans le cas d'une base de code monolithique, elle l'est encore plus au niveau des données.

L'automatisation est la clé, la migration doit être progressive afin d'éviter les pertes de données en cas de problème.

Sensibilisation aux bonnes pratiques techniques du Software Craftsmanship : Lego® à la rescousse !
---
- Isabelle Blasquez (@iblasquez) - Associate Professor in Software Engineering #iutagile - @duchessfr - @CodeWeekEU #SoftwareCraftsmanship @MuseomixLIM - Limoges
- Alice Barralon - Artisan agiliste et développeur sur ses temps libres.

Slides : https://github.com/iblasquez/atelier-bonnes-pratiques-tdd-lego

Cet atelier permet de passer en revue une série de bonnes pratiques issues du Software Craftmanship :

- Le KISS : Keep It Simple, Stupid
- Le YAGNI : You Ain't Gonna Need It
- L'intérêt du refactoring en TDD.
- L'indispensable communication entre équipes en vue d'une intégration de projet multi-modules.
- L'apport des tests pour décrire un projet.

En plus d'illustrer au développeur les bonnes pratiques du développement, il permet d'illustrer à un non développeur les contraintes de notre métier.

Vue.js, à l'ombre des géants
---
- Cyril Balit (@cbalit) - Front End CTO @sfeir GDE @google HTML5, JS, Web Components, Angular, Polymer, @bestofwebconf organizer...

Présentation des possibilité de VueJS :
- VueJS peut être intégré directement dans une page HTML existante de façon très simple.
- VueJS peut également être utilisé avec Webpack et ainsi profiter des outils du monde Javascript.
