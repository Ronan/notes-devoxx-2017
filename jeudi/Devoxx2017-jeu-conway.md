Loi de conway
===
Clément Rochas - Xebia - @crochas
Coach Agile

L'organisation des équipes a un effet direct sur le code qu'elle produit.

SI sédimentaire : plein de projets, les uns après les autres, sur des budgets cadrés.

Ouvriers Qualifiés : Pas de réflection de la part du développeur, framework interne pour générer des trucs.
Les gens oublient de réfléchir. Pas d'amélioration continue du fait de l'organisation des équipes.

Equipes spécialisées : "Tout ressemble à un clou pour qui ne possède qu'un marteau."


