DevoxxFR 2017 - Mercredi
===

Préparez-vous à la modularité selon Java 9
---
- Alexis Hassler (@AlexisHassler) - Développeur Java, formateur indépendant, JBoss Hero
- Remi Forax (http://forax.github.io/) - Université Paris Est Marne-la-vallée

Slides : http://prez.sewatech.fr/devoxxfr17-uni/

Le Java 9 va adresser les problèmes de Java suivants :

- Le compilateur travaille sur un DAG (Graphe Orienté Acyclique), alors que le runtime travaille sur des dépendances de façon linéaire. Ce qui peut être la source de conflits.
- Des jar de Java commence à être volumineuses (rt.jar - 66Mo).
- En cas de problème de sécurité, seules les applications utilisant le dit module seront impactés, et il n'y aura besoin de mettre à jour que le module en question.

Le JDK 9 va modifier quelques pratiques de développement, avec la notion de modules.
Ces modules nous permettront de mieux gérer la visibilité des classes entre modules.
Les "modules automatiques" pourrons nous aider à gérer la transition, mais attention, pour bien fonctionner les noms des jar en dépendance doivent être "normés".

Les IDE et outils de builds comme Maven sont en train de se mettre à jour pour gérer les modules correctement.

Il devient urgent de moderniser notre processus de build en utilisant un outil comme Maven car la ligne de commande de build se complexifie, et le paramétrer avec Ant risque de devenir un enfer. Il sera également sans doute nécessaire de mettre à jour nos dépendances "anciennes" comme Axis et Hibernate.

Java 9 est prévue pour sortir en Juillet 2017. Il serait bien de prévoir du temps à la fin de l'année 2017 pour démarrer ce chantier.

[Notes](Devoxx2017-mer-jdk9.md)

L'API Collector dans tous ses états
---
- José Paumard (@JosePaumard) - Java Champion, JavaOne Rockstar, Java enthousiast, blogger sur Java le Soir (http://blog.paumard.org/)

Slides : https://www.slideshare.net/jpaumard/lapi-collector-dans-tous-ses-tats-74528312

Présentation théorique sur la construction de collectors réutilisables.

La difficulté reste de faire du code lisible et maintenable pour qui n'est pas habitué à la programmation fonctionnelle, mais la présentation montre des astuces à utiliser en ce sens.

Par contre l'API est très performante sur la gestion de grosses quantité de données :
- En terme d'emprunte mémoire car on ne charge les données que quand on en a besoin, et on peut s'en débarrasser très rapidement, et on ne gère que peu de strutures intermédiaires contrairement à l'impératif
- En terme de CPU car si le code est bien optimisé, on n'effectue les traitements que sur les structures le nécessitant, de plus ceci se parallélise très bien.

Je suis persuadé que pour nos calculs réalisés en PLSQL, on gagnerait en pérénité, maintenabilité et performance à basculer sur des traitements Java utilisant ces API introduites en Java 8.

[Notes](Devoxx2017-mer-collectors.md)

CSS is Awesome!
---
- Igor Laborie (@ilaborie) - Expert Java & Web @MonkeyPatch_io (https://ilaborie.org/)

Slides : https://ilaborie.github.io/css-awesome/devoxx.html

La force de cette présentation est qu'elle fait un tour d'horizon impressionnant sur ce qu'il est possible de réaliser en pur HTML5/CSS3 sans aucun code Javascript ni images.
Enormement de liens sont fournis.

[Notes](Devoxx2017-mer-css.md)

Boîte à outils d'investigation des soucis de mémoire
---
- Jean Bisutti (@jean_bisutti) - Developer #softwareCraftsmanship #agile

- Slides : https://www.slideshare.net/JeanBisutti/bote-outils-dinvestigation-des-soucis-de-mmoire-74483013

- Github : https://github.com/jeanbisutti/memory-investigation

Un tour d'horizon des outils d'analyse du comportement de la mémoire dans un JVM. Notament de l'outil GCViewer. La présentation est réalisée sur un cas d'école d'enlisement mémoire ou les objets ne peuvent être libérés par le GC et trop d'allocations sont réalisées.

[Notes](Devoxx2017-mer-jvmmemory.md)

JUnit-docker
---
- Xavier Detant (@XDetant) - From SOAT
- Vincent Demeester (@vdemeest) - French developer, sysadmin, factotum and free-software fan. Working @docker, ex @ZenikaIT, @Docker 🐳 trainer & maintainer. Unicode lover

- Outil : https://faustxvi.github.io/junit5-docker/

Présentation de l'outil junit5-docker qui permet d'allumer une image docker pour la réalisation de test d'intégration et ainsi de gagner en stabilité sur ceux-ci.

[Notes](Devoxx2017-mer-Junit5-Docker.md)
