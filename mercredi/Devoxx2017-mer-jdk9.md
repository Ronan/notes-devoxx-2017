Devoxx 2017 - Jour 1
===

Préparez vous à la modularité selon Java 9
---
Alexis Hassler, Rémi Forax

Problèmes :

- A la compil on voit un DAG, a l'execution c'est linéaire : Conflits
- Des jar deviennent un peu grosse : rt.jar - 66Mo
- Modularité pour la sécurité.

Modular JDK :

module central : java.base

Java 6 : plein de choses montent de JavaEE
Java 9 : deprecated, ca redescend dans JavaEE

JDK module non standard : sun.* / com.sun.*
Supprimés ou planqués
Deviennent des modules internes : jdk.*

Modules descriptor

''''
   /* module-info.java */
   module app.main {
      requires java.base; // optionnel
      requires java.sql;
      requires java.management;
   }
''''

Module = jar + module-info.class

Module path = dossier contenant les modules.
Addition au class path. Le but est de faire disparaitre à terme le classpath.

Execution :
- Bloque si on a 2x le meme module dans le module path.
-  "open module" si on veut aller fouiller dans les private & co.

Projet multi module : indiquer "export".

Layout multi module : compris pas javac mais pas par Maven/Gradle et les IDE.

Pour la JDK, il n'y a pas de notion de versions.
Elle interdit juste d'avoir 2x le meme module.

Toute class public doit etre dans un package exporté par le module.

export et export to module(s)

deep reflection : setAccessible(true) => Plante.
Pour que ca passe, utiliser "open" / "opens".

requires transitive : 1 niveau
requires static : compilation mais pas runtime

Attention aux dates des articles, l'implementation a commencée il y longtemps, et changé beaucoup depuis 10 ans.

Modules automatiques : Voir un jar 'old' dans un module.
Solution merdique basée sur le nom du jar qui doit être normé (old-lib-X.Y).
Le module automatique à le droit de dépendre de trucs du class path.

java.util.ServiceLoader : injection de dépendances depuis JAVA 6
dans module-info : "uses api" / "provides api with impl"

Compiling in Java 8, running in Java 9 : Des trucs vont casser.
flag : --permit-illegal-access permet de les faire.
N'existera plus en JDK10.

--add-reads / --add-exports / --add-opens : ajouter les options dans module info

outil d'analyse static : jdeps --jdk-internals
Existe en jdk8 : jdeps -jdkinternals

Testabilité :

1. classe de test dans un module donc package différent

2. classe de test dans un module différent mais pas un package

On ne met pas de module-info dans le dossier des tests. Option --patch-module.

Conclusion : Les IDE et les outils de build sont en train de se mettre à niveau.
Les lignes de commande de build en transition se complexifient. Maven, vite !

Intérêt :
- Code fermé, on sait de quoi on dépend, patchs de sécurité, on sait ce dont on a besoin.
- jlink : construire son exécutable natif, plus petit. Attention, il refuse les mdoules automatiques.
- jaotc : compiler le code en .so, .dll, .dynlib.

Date de sortie : Juillet 2017
